## Regular change

<!-- Regular change, to be approved by the CAB. -->

/label ~"Change::Regular" ~CAB ~"CAB::to-approve"

## Summary
<!-- Outline the issue being faced, and why this required a change !-->

## Area of the system
<!-- This might only be one part, but may involve multiple sections !-->

## How does this currently work?
<!-- the current process, and any associated business rules !-->

## What is the desired way of working?
<!-- after the change, what should the process be, and what should the business rules be !-->

## Rollback plan ?
<!-- describe how to rollback the change in case the expected change is not working -->

## Priority/Severity
<!-- Delete as appropriate. The priority and severity assigned may be different to this !-->
- [ ] High (This will bring a huge increase in performance/productivity/usability, or is a legislative requirement)
- [ ] Medium (This will bring a good increase in performance/productivity/usability)
- [ ] Low (anything else e.g., trivial, minor improvements)


## Severity
<!-- Pick one -->
<!-- /label ~"Severity::1-Critical" --> 
<!-- /label ~"Severity::2-Major" -->
<!-- /label ~"Severity::3-Moderate" -->
<!-- /label ~"Severity::4-Minor" -->
<!-- /label ~"Severity::5-Cosmetic" -->
